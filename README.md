Overview
========

UNSWDEM is an implementation of the terrain analysis
algorithm described in
[Wang et al. 2010, Morphometric characterisation of landform from DEMs.
International Journal of Geographical Information Science,
24, 305-326](http://doi.org/10.1080/13658810802467969).

It is implemented using ArcPy, mostly in 2010.

It currently needs some attention as it does not work cleanly under ArcGIS 10.6.
