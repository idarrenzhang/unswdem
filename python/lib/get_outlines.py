#!/usr/bin/env python

# Import native arcpy module and other required modules
import argparse
import os
import sys
import arcpy


# def create_poly(e, poly_array, poly_pnt):
#     return feat

def get_outlines(glob_pattern, out_poly_name, raster_workspace, resolution=1000):
    arcpy.env.overwriteOutput = True
    if not arcpy.env.workspace:
        if raster_workspace:
            arcpy.env.workspace = raster_workspace
        else:
            arcpy.env.workspace = os.getcwd()
    print arcpy.env.workspace

    if not out_poly_name:
        out_poly_name = arcpy.CreateScratchName("tilepoly", "", "shapefile", raster_workspace)

    files = arcpy.ListRasters(glob_pattern)
    if not files:
        arcpy.AddError("No raster files to list.")
        sys.exit()

    #  use coordsys defined by first raster
    coordsys = arcpy.Describe(files[0]).SpatialReference

    #coordsys = r"Coordinate Systems\Geographic Coordinate Systems\World\WGS 1984.prj"
    #point_coordsys = os.path.join(os.environ['ARCGISHOME'], coordsys)

    #out_poly_name = "bounds.shp"
    # if not out_poly_name:
    #     out_poly_name = arcpy.CreateScratchName("tilepoly", "", datatype="Shapefile", workspace=raster_workspace)
    # # out_poly_name = out_dir + out_base
    out_poly_name = os.path.abspath(out_poly_name)
    out_dir  = os.path.dirname (out_poly_name)
    out_base = os.path.basename(out_poly_name)
    print out_dir, out_base
    # ??? '#'
    arcpy.CreateFeatureclass_management(out_dir, out_base, "Polygon")
    arcpy.DefineProjection_management(out_poly_name, coordsys)
    arcpy.MakeFeatureLayer_management(out_poly_name, 'poly_lyr')
    file_fld = "filename"

    # Adds a new field to a table or the table of a feature class, feature layer, and/or rasters with attribute tables.
    # AddField_management (in_table, field_name, field_type...}
    arcpy.AddField_management(out_poly_name, file_fld, "TEXT")
    print "Added field '%s' to %s" % (file_fld, out_poly_name)

    # Initialize variables for keeping track of feature ID.
    point_id = -1
    poly_id     = -1

    for file in files:
        #print file
        try:
            desc = arcpy.Describe(file)
            e    = desc.extent
            c    = desc.MeanCellHeight
            cell_res = c * resolution

            #  default polygon resolution is whole file
            #  otherwise convert the resolution arg to cell units
            #  (assumes square cells)
            # but resolution will never be none?? (original: if not resolution)
            if not (resolution==1000):
                cell_res = max (e.XMax - e.XMin, e.YMax - e.YMin)
                print "Calculated default resolution %s" % cell_res

            xleft  = e.XMin
            while xleft < e.XMax:
                xright = min (e.XMax, xleft + cell_res)
                ylower = e.YMin
                while ylower < e.YMax:
                    yupper = min (e.YMax, ylower + cell_res)
                    #print ylower, yupper
                    coords = [  [xleft,  ylower],
                                [xright, ylower],
                                [xright, yupper],
                                [xleft,  yupper],
                                [xleft,  ylower],
                             ]
                    #print file, coords

                    poly_id  += 1
                    poly_array = arcpy.Array()
                    for coord in coords:
                        point = arcpy.Point(coord[0], coord[1], point_id)
                        poly_array.add(point)
                        point_id += 1

                    # Create a new row or feature in the feature class
                    # and add the points
                    # Open insert cursor for the new feature class
                    polygon = arcpy.Polygon(poly_array)
                    cur = arcpy.da.InsertCursor(out_poly_name, ['Shape@', 'ID', file_fld])

                    cur.insertRow([polygon, poly_id, file])

                    # feat       = cur.newRow()
                    # feat.shape = poly_array
                    # feat.ID    = poly_id
                    #
                    # feat.SetValue(file_fld, file)
                    #
                    # cur.insertRow(feat)

                    #  and now clear them from the array
                    poly_array.removeAll()
                    ylower = yupper
                xleft = xright

        except:
            try:
                del cur
            except:
                pass

            raise

    del cur
    print "Done"

    return out_poly_name

# def get_args():
#     parser = argparse.ArgumentParser(description='Get_outlines', prog='Input')
#     parser.add_argument('glob_pattern', type=str, help='globber')
#     parser.add_argument('out_poly_name', type=str, help='give a polygon file name')
#     parser.add_argument('-s', '--raster_workspace', type=str, help='raster_workspace', default=None)
#     parser.add_argument('-r', '--resolution', type=float, help='resolution in cells')
#     args = parser.parse_args()
#     glob_pattern = args.globber
#     out_poly_name = args.out_poly_name
#     workspace = os.path.abspath(args.raster_workspace)
#     resolution = args.resolution
#     return glob_pattern, out_poly_name, workspace, resolution
#
# if __name__ == "__main__":
#
#     #  read in parameters.
#     #  We need:
#     #    Input wildcard (glob pattern)
#     #    Output polygon file name
#     #    Input workspace (optional)
#     #    Resolution in cells (optional)
#     try:
#         glob_pattern, out_poly_name, raster_workspace, resolution = get_args()
#     except:
#         print 'Input is not valid'
#         sys.exit()
#
#     get_outlines(glob_pattern, out_poly_name, raster_workspace, resolution)
