#!/usr/bin/env python
import json
from unswdem import *

'''
A script to generate all test cases results.

Each test case will take up 24 lines
1 line: "case X" (title, indication only)
22 lines: parameters --> line [1: 23]
    >> first 3 lines are indication only, just parameters passing to the case (type, invert, offset)
    >> The other 19 lines are testing parameters
1 line: '\n' separator              

'''



# proposed parameters
radius, offsets, inverts, types, rots
radius = 5
# proposed 180 tests
offsets = ((0,0), (1, 1), (2, 0), (0, 2), (0, 10), (9, 10))
inverts = (False, True)
types = ('elliptic', 'hyperbolic', 'parabolic', 'planar', 'flat')
rots = (0, 1, 2)

# single case - for testing purpose
# offsets = ((0, 2),)
# inverts = (True,)
# types = ('planar',)
# rots = (1,)

with open('test_cases.txt', 'w') as f:
    # f.write('start of the tests\n')
    num = 0
    for rot in rots:
        for invert in inverts:
            for type in types:
                for offset in offsets:
                    dem = UNSWDEM(radius=radius)
                    dem.offset = offset
                    dem.generate_test_data(invert=invert, type=type, rot=rot)
                    dem.fit_model()
                    dem.rotate_coeffs()
                    dem.get_morphtype()
                    dem.get_morphclass()
                    #  test stuff starts here
                    # 3 + 18 +1 = 22 testing parameters
                    num += 1
                    f.write('Case %i:\n' % num)
                    test_parameters = [type, invert, offset, rot, dem.get_max_z_coord(), dem.get_min_z_coord(),
                                       dem.coeffs,
                                       dem.r2, dem.centre,
                                       dem.parallel, dem.theta, dem.morphval, dem.morphclass, dem.morphtype,
                                       dem.dist, dem.axes, dem.dist_ax1, dem.dist_ax2, dem.muval, dem.cmti,
                                       dem.data_to_ascii()]
                    # print dem.centre
                    for e in test_parameters:
                        f.write(json.dumps(e) + '\n')
                    try:
                        dist = (dem.get_dist_p2l(line_array=dem.axes[0], x0=offset[0], y0=offset[1]),
                                dem.get_dist_p2l(line_array=dem.axes[1], x0=offset[0], y0=offset[1]))
                        f.write(json.dumps(dist) + '\n')
                    except:
                        f.write('null\n')
                    f.write('\n')

