import unittest
from unswdem import *
import json

'''
A modified version of unittest for unswdem
Each test case is automatically loaded from the generated file by a text generator
'''


class Parameters(unittest.TestCase):
    def __init__(self, methodName='Test_One', param=None):
        super(Parameters, self).__init__(methodName)
        self.test_case = param

    @staticmethod
    def parametrize(testcase_class, param=None):
        testloader = unittest.TestLoader()
        testnames = testloader.getTestCaseNames(testcase_class)
        suite = unittest.TestSuite()
        for name in testnames:
            suite.addTest(testcase_class(name, param=param))
        return suite


class Test_One(Parameters):
    def assertEverythingAlmostEqual(self, first, second, places=6, msg=None):
        if isinstance(second, type(None)):
            self.assertIsNone(first)
        if isinstance(second, (int, float)):
            self.assertAlmostEqual(first, second, places=places)
        elif isinstance(second, (list, tuple)):
            self.assertEqual(len(first), len(second))
            for a, b in zip(first, second):
                self.assertEverythingAlmostEqual(a, b)
        elif isinstance(second, dict):
            self.assertEqual(set(first), set(second))
            for key in second:
                self.assertEverythingAlmostEqual(first[key], second[key])
        else:
            self.assertEqual(first, second)

    def setUp(self):
        self.test_instance = UNSWDEM()
        # parameters
        all_variables = ['types', 'invert', 'offset', 'rot', 'max', 'min', 'coeffs', 'r2', 'centre', 'parallel',
                              'theta', 'morphval', 'morphclass', 'morphtype', 'dist', 'axes', 'dist_ax1', 'dist_ax2',
                              'muval', 'cmti', 'ascii', 'distp2l']
        # test_case = []
        for i in range(len(self.test_case)):
            setattr(self, all_variables[i], json.loads(self.test_case[i].rstrip('\n')))

        # initialization
        self.test_instance.offset = self.offset
        self.test_instance.generate_test_data(invert=self.invert, type=self.types, rot=self.rot)
        self.test_instance.fit_model()
        self.test_instance.rotate_coeffs()
        self.test_instance.get_morphtype()
        self.test_instance.get_morphclass()

        # print self.test_instance.parallel
        # print self.test_instance.axes

    def tearDown(self):
        self.test_instance.reset()

    def test_extent(self):
        max = list(self.test_instance.get_max_z_coord())
        min = list(self.test_instance.get_min_z_coord())
        self.assertEverythingAlmostEqual(max, self.max, msg='max is not correct')
        self.assertEverythingAlmostEqual(min, self.min, msg='min is not correct')

    def test_coeffs(self):
        self.assertEverythingAlmostEqual(self.test_instance.coeffs, self.coeffs,
                                         msg='coefficients are not correct')

    def test_r2(self):
        self.assertAlmostEqual(self.test_instance.r2, self.r2, places=6, msg='r2 is not correct')

    def test_centre(self):
        # centre = "(%.3f, %.3f)" % (self.test_instance.centre[0], self.test_instance.centre[1])
        self.assertEverythingAlmostEqual(self.test_instance.centre, self.centre,
                                         'centre is not correct %s' % str(self.centre))
        # print self.test_instance.centre

    def test_angle(self):
        self.assertEqual(self.test_instance.parallel, self.parallel, 'should be parallel')
        self.assertAlmostEqual(self.test_instance.theta, self.theta, places=6, msg='theta is wrong')

    def test_morph(self):
        self.assertAlmostEqual(self.test_instance.morphval, self.morphval, places=6, msg='morphval is wrong')
        self.assertEqual(self.test_instance.morphclass, self.morphclass)
        self.assertEqual(self.test_instance.morphtype, self.morphtype)
        # print self.test_instance.morphval


    def test_dist(self):
        # print self.test_instance.dist
        # print self.dist

        self.assertEverythingAlmostEqual(self.test_instance.dist, self.dist, msg='dist is not right')

    def test_axes(self):
        # print self.test_instance.axes
        # print self.axes
        self.assertEverythingAlmostEqual(self.test_instance.axes, self.axes, msg='axes is not right')
        self.assertEverythingAlmostEqual(self.test_instance.dist_ax1, self.dist_ax1, msg='ax1 not correct')
        self.assertEverythingAlmostEqual(self.test_instance.dist_ax2, self.dist_ax2, msg='ax2 not correct')

    def test_muvals(self):
        self.assertEverythingAlmostEqual(self.test_instance.muval, self.muval, places=3, msg='muvals are not correct')

    def test_cmti(self):
        # print self.test_case
        # print self.test_instance.cmti
        self.assertAlmostEqual(self.test_instance.cmti, self.cmti)

    def test_ascii(self):
        self.ascii = {int(k): {int(k): kv for k, kv in v.iteritems()} for k, v in self.ascii.iteritems()}
        self.assertEverythingAlmostEqual(self.test_instance.data_to_ascii(), self.ascii)

    def test_dist_offset(self):
        try:
            distp2l = list((
                self.test_instance.get_dist_p2l(self.test_instance.axes[0], x0=self.offset[0], y0=self.offset[1]),
                self.test_instance.get_dist_p2l(self.test_instance.axes[1], x0=self.offset[0], y0=self.offset[1])))
        except:
            distp2l = None
        self.assertEverythingAlmostEqual(distp2l, self.distp2l)


if __name__ == '__main__':
    seq = 0
    with open('test_cases.txt', 'r') as f:
        lines = f.readlines()
        while lines[24 * seq + 1: 24 * seq + 23]:
            cases = lines[24 * seq + 1: 24 * seq + 23]
            print 'Test %i\n' %(seq+1)
            # remove verbosity=2 to make it less verbose
            unittest.TextTestRunner(verbosity=2).run(Parameters.parametrize(Test_One, param=cases))
            seq += 1
